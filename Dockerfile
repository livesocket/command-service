FROM golang:1.12-alpine as base
RUN apk --no-cache add git ca-certificates g++
WORKDIR /repos/command-service
ADD go.mod go.sum ./
RUN go mod download

FROM base as builder
WORKDIR /repos/command-service
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -o command-service

FROM scratch as release
COPY --from=builder /repos/command-service/command-service /command-service
EXPOSE 8080
ENTRYPOINT ["/command-service"]