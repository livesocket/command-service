package topics

import (
	"log"

	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/service/v2"
	"gitlab.com/livesocket/service/v2/socket"
)

func ChannelDestroyed(service *service.Service) func(*socket.Event) {
	return func(event *wamp.Event) {
		if event.ArgumentsKw["channel"] == nil {
			log.Print("ChannelDestroyed: No channel")
		}

		channel := event.ArgumentsKw["channel"]
		_, err := service.SimpleCall("private.command.destroyAll", nil, wamp.Dict{"channel": channel})
		if err != nil {
			log.Print(err)
		}
	}
}
